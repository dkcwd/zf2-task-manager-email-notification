<?php
/**
 * Define constants for test purposes
 */
defined('ZF2_TASK_MANAGER_EMAIL_NOTIFICATION_TEST_MODE')
|| define('ZF2_TASK_MANAGER_EMAIL_NOTIFICATION_TEST_MODE', false);

defined('ZF2_TASK_MANAGER_EMAIL_NOTIFICATION_TEST_OUTPUT_PATH')
|| define('ZF2_TASK_MANAGER_EMAIL_NOTIFICATION_TEST_OUTPUT_PATH', sys_get_temp_dir());

/**
 * Note that dkcwd/zf2-task-manager uses SlmQueue.
 *
 * dkcwd/zf2-task-manager-email-notifications notification tasks
 * are ultimately based on the AbstractJob class allowing for easy
 * queuing and execution.
 *
 * Redefine the following arrays in your local config, note the recommended approach
 * is that you use a factory class to instantiate the notification task with relevant
 * dependencies
 */
return array(
    'view_manager' => array(
        'template_map' => array(
            // 'friendly_name_for_email_template' => 'location/within/file/system/for/the/email/template'
        )
    ),
    'task_manager' => array(
        // each notification task should be registered as a friendly name and class name pair
        'registered_tasks' => array(
            // 'friendly_name_for_notification' => 'Fully\Qualified\Class\Name\Of\Notification\Task',
        ),
    ),
    'slm_queue' => array(
        'job_manager' => array(
            'factories' => array(
                // 'Fully\Qualified\Class\Name\Of\Notification\Task' => 'Fully\Qualified\Class\Name\Of\Notification\Task\Factory',
            ),
        ),
    ),
    'mail' => array(
        'transport' => array(
            'options' => array(
                'host' => 'your-host',
                'connection_class' => 'login',
                'connection_config' => array(
                    'username' => 'yourUsername',
                    'password' => 'yourPassword',
                    'ssl' => 'tls'
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zf2TaskManagerEmailNotification\Mail\TransportService' => 'Zf2TaskManagerEmailNotification\Mail\Service\SmtpTransportFactory',
        ),
    ),
);