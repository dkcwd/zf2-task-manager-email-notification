<?php

namespace Zf2TaskManagerEmailNotification\Mail\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

class SmtpTransportFactory implements FactoryInterface
{
    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return Smtp
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        if (! isset($config['mail'])) {
            throw new \RuntimeException('Expected mail configuration to be set');
        }

        if (! isset($config['mail']['transport'])) {
            throw new \RuntimeException('Expected mail transport configuration to be set');
        }

        if (! isset($config['mail']['transport']['options'])) {
            throw new \RuntimeException('Expected mail transport options to be set');
        }

        $transport = new Smtp();
        $transport->setOptions(new SmtpOptions($config['mail']['transport']['options']));

        return $transport;
    }
}