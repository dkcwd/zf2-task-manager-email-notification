<?php

namespace Zf2TaskManagerEmailNotification\Task;

use SlmQueue\Job\AbstractJob;

use Zend\Mime\Mime as Mime;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Message as MailMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\Mail\AddressList;
use Zend\Mail\Transport\TransportInterface;

abstract class AbstractEmailNotificationTask extends AbstractJob
{
    /** @var TransportInterface */
    protected $transport;

    /** @var PhpRenderer  */
    protected $phpRenderer;

    /** @var ViewModel */
    protected $viewModel;

    /** @var AddressList */
    protected $addressList;

    /** @var array */
    protected $templateData;

    /** @var string */
    protected $template;

    /** @var array */
    protected $attachments;

    /** @var string */
    protected $fromEmail;

    /** @var string */
    protected $fromName;

    /** @var string */
    protected $subject;

    /**
     * @param TransportInterface $transport
     */
    public function setTransport(TransportInterface $transport)
    {
        $this->transport = $transport;
    }

    /**
     * @return TransportInterface
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param PhpRenderer $phpRenderer
     */
    public function setPhpRenderer(PhpRenderer $phpRenderer)
    {
        $this->phpRenderer = $phpRenderer;
    }

    /**
     * @return PhpRenderer
     */
    public function getPhpRenderer()
    {
        return $this->phpRenderer;
    }

    /**
     * @param ViewModel $viewModel
     */
    public function setViewModel(ViewModel $viewModel)
    {
        $this->viewModel = $viewModel;
    }

    /**
     * @return ViewModel
     */
    public function getViewModel()
    {
        return $this->viewModel;
    }

    /**
     * @return array
     */
    public function getTemplateData()
    {
        return $this->templateData;
    }

    /**
     * @param array $templateData
     */
    public function setTemplateData(array $templateData)
    {
        $this->templateData = $templateData;
    }

    /**
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return AddressList
     */
    public function getAddressList()
    {
        return $this->addressList;
    }

    /**
     * @param AddressList $addressList
     */
    public function setAddressList(AddressList $addressList)
    {
        $this->addressList = $addressList;
    }

    /**
     * @return string
     */
    protected function renderEmail()
    {
        $model = $this->getViewModel();
        $model->setVariables($this->getTemplateData());

        $renderer = $this->getPhpRenderer();

        return $renderer->render($model);
    }

    /**
     * @return array
     */
    public function getAttachments()
    {
        return is_array($this->attachments) ? $this->attachments : array();
    }

    /**
     * @param array $attachments
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param string $fromName
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Prepares the notification template with variable values
     * @return void
     */
    abstract public function prepare();

    public function execute()
    {
        $this->prepare();
        $content = $this->renderEmail();

        // for debugging html email content
        if (ZF2_TASK_MANAGER_EMAIL_NOTIFICATION_TEST_MODE) {
            file_put_contents(
                basename(ZF2_TASK_MANAGER_EMAIL_NOTIFICATION_TEST_OUTPUT_PATH) . '/' . time() . '-test-output-email.html',
                $content
            );
        }

        // first create the parts
        $text = new MimePart($content);

        $text->type = Mime::TYPE_HTML;
        $text->charset = 'utf-8';

        $attachments = array();
        $attachments[] = $text;

        foreach ($this->getAttachments() as $attachment) {
            if ($attachment instanceof MimePart) {
                $attachments[] = $attachment;
            }
            continue;
        }

        // then add them to a MIME message
        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts($attachments);

        $message = new MailMessage();

        $message->setBody($mimeMessage);
        $message->setFrom($this->getFromEmail(), $this->getFromName());
        $message->addTo($this->getAddressList());
        $message->setSubject($this->getSubject());

        $transport = $this->getTransport();
        $transport->send($message);

        return true;
    }
}