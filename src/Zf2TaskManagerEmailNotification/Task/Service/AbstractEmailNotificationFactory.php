<?php

namespace Zf2TaskManagerEmailNotification\Task\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Model\ViewModel;

abstract class AbstractEmailNotificationFactory implements FactoryInterface
{
    /** @var mixed */
    protected $renderer;

    /** @var mixed */
    protected $viewModel;

    /** @var mixed */
    protected $transport;

    /**
     * @return mixed
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param mixed $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return mixed
     */
    public function getViewModel()
    {
        return $this->viewModel;
    }

    /**
     * @param mixed $viewModel
     */
    public function setViewModel($viewModel)
    {
        $this->viewModel = $viewModel;
    }

    /**
     * @return mixed
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param mixed $transport
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /**
         * Some base setup:
         *
         *  - set renderer for notification templates
         *   - set the template resolver
         *   - set the email transport
         *
         * Each derived email notification task factory can use this initialisation logic
         * e.g. in your notification task factory do:
         *     parent::createService($serviceLocator)
         */
        $serviceLocator = $serviceLocator->getServiceLocator();
        $this->setTransport($serviceLocator->get('Zf2TaskManagerEmailNotification\Mail\TransportService'));
        $this->setRenderer(new PhpRenderer());
        $this->getRenderer()->setResolver($serviceLocator->get('ViewResolver'));
        $this->setViewModel(new ViewModel());
    }
}