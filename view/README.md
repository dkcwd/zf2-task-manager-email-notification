# README #

The templates for each email can be simple .phtml files.
They can contain all markup required for the email with PHP placeholders like `<?php echo $this->userFirstName; ?>`.

Alternatively you can have a default header and footer which the template will include.
